<?php require_once('includes/session.php');
      require_once('includes/conn.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Traffic Fine Management System - DASHBOARD</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/awesome/font-awesome.css">
        <link rel="stylesheet" href="assets/css/animate.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar" class="sammacmedia">
                <div class="sidebar-header">
                    <h3>Traffic Fine Management System</h3>
                    <strong>Traffic Fine Management System</strong>
                </div>

                <ul class="list-unstyled components">
                    <li>
                        <a href="dashboard.php">
                            <i class="fa fa-th"></i>
                           Dashboard
                        </a>
                    </li>

                   
                 <?php

                   
                   // Admin Access Level
                   
                    if($_SESSION['permission']==1){

                    ?>
                    <li>
                        <a href="a_users.php">
                            <i class="fa fa-user"></i>
                            Add Officers
                        </a>
                    </li>
                    
                     <li>
                        <a href="ViolationCat.php">
                            <i class="fa fa-plus"></i>
                             Add Traffic Violation Types
                        </a>
                    </li>


                    <li>
                        <a href="v_users.php">
                            <i class="fa fa-table"></i>
                            View Officers
                        </a>
                    </li>



                     <li>
                        <a href="paymentview.php">
                            <i class="fa fa-table"></i>
                             View Payment Records
                        </a>
                    </li>

                     <li>
                        <a href="show_payment.php">
                            <i class="fa fa-table"></i>
                             Statistical Data
                        </a>
                    </li>




                    <?php }

                    ?>


                   <?php
                   
                   // OIC Access Level
                   
                    if($_SESSION['permission']==2){

                    ?>
  
                    <li>
                        <a href="v_users.php">
                            <i class="fa fa-table"></i>
                            View Officers
                        </a>
                    </li>






                    <?php }

                    ?>


                   <?php
                   
                   // PO Access Level
                   
                    if($_SESSION['permission']==3){

                    ?>
  


                    <?php }

                    ?>

                                        <li>
                        <a href="v_ViolationCat.php">
                            <i class="fa fa-table"></i>
                             View Violation Categories
                        </a>
                    </li>

                    <li>
                        <a href="tickets.php">
                            <i class="fa fa-plus"></i>
                             Issue Fine Ticket
                        </a>
                    </li>

                    <li>
                        <a href="payment.php">
                            <i class="fa fa-plus"></i>
                             Issue Fine Payment
                        </a>
                    </li>



                    
                    <li>
                        <a href="settings.php">
                            <i class="fa fa-cog"></i>
                            Settings
                        </a>
                    </li>
                </ul>
            </nav>


            <!-- Page Content Holder -->
            <div id="content">
             
                <div clas="col-md-12 sammacmedia">
                    <img src="assets/image/ssm.jpg" class="img-thumbnail">
                    </div>
         
                
                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header" id="sams">
                            <button type="button" id="sidebarCollapse" id="makota" class="btn btn-sam animated tada navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Menu</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right  makotasamuel">
                                <li><a href="#"><?php require_once('includes/name.php');?></a></li>
                                <li ><a href="logout.php"><i class="fa fa-power-off"> Logout</i></a></li>
           
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="line"></div>
                                            <?php
                            if(isset($mysqli,$_POST['submit'])){

                            $idTrafficViolation = mysqli_real_escape_string($mysqli,$_POST['idTrafficViolation']);
                            $FinePayment = mysqli_real_escape_string($mysqli,$_POST['FinePayment']);
                            $TimeStamp = date(" d M Y ");

                            $payeementeeNIC = mysqli_real_escape_string($mysqli,$_POST['payeementeeNIC']);
                            $PaymenteeFName = mysqli_real_escape_string($mysqli,$_POST['PaymenteeFName']); 
                            $PaymenteeLName = mysqli_real_escape_string($mysqli,$_POST['PaymenteeLName']); 
                            $payementeeAddress  = mysqli_real_escape_string($mysqli,$_POST['payementeeAddress']);
                        


  
                           
                            

                            $sql_pay = "SELECT * FROM trafficfinepayment WHERE idTrafficViolation ='$idTrafficViolation'";
                            $res_pay = mysqli_query($mysqli, $sql_pay);

                  
                $sql = "INSERT INTO trafficfinepayment(idTrafficViolation,FinePayment,TimeStamp,payeementeeNIC,PaymenteeFName,PaymenteeLName,payementeeAddress)VALUES('$idTrafficViolation','$FinePayment','$TimeStamp','$payeementeeNIC','$PaymenteeFName','$PaymenteeLName','$payementeeAddress')";
                $results = mysqli_query($mysqli,$sql);
                        
                        
                        
                        if($results==1){
                              ?>
                        <div class="alert alert-success strover animated bounce" id="sams1">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong> Successfully! </strong><?php echo'Thank you for creating account';?></div>
                        <?php

                          }else{
                                ?>
                         <div id="sams1" class="sufee-alert alert with-close alert-danger alert-dismissible fade show col-lg-12">
											<span class="badge badge-pill badge-danger">Error</span>
											OOPS something went wrong
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
                        <?php    
                          }      
                      }
                 
            
                
                ?>
		<div class="panel panel-default sammacmedia">
            <div class="panel-heading">Online Fine Payment</div>
        <div class="panel-body">
            <form method="post" action="payment.php">
        <div class="row form-group">
         
            <div class="col-lg-6">
            <label>Fine Record ID</label>
              <input type="numbers" class="form-control" name="idTrafficViolation" >
            </div>  
          <div class="col-lg-6">
            <label>Fine Payment</label>
              <input type="numbers" class="form-control" name="FinePayment"  required>
            </div>  
             <div class="col-lg-6">
            <label>Payeementee NIC</label>
              <input type="text" class="form-control" name="payeementeeNIC"  required>
            </div>  
        
         
          <div class="col-lg-6">
            <label>Payeementee First Name</label>
              <input type="text" class="form-control" name="PaymenteeFName" required>
            </div>  

            
          <div class="col-lg-6">
            <label>Payeementee Last Name</label>
              <input type="text" class="form-control" name="PaymenteeLName" required>
            </div>  
             <div class="col-lg-6">
            <label>Payeementee Address</label>
              <input type="text" class="form-control" name="payementeeAddress" pattern="[A-Za-z]{3,}" required>
            </div>  
        </div>   
     
                <div class="row">
                <div class="col-md-6">
                  <button type="submit" name="submit" class="btn btn-suc btn-block"><span class="fa fa-plus"></span> Process</button>  
                </div>
                     <div class="col-md-6">
                  <button type="reset" class="btn btn-dan btn-block"><span class="fa fa-times"></span> Cancel</button>  
                </div>
                </div>
            </form>

            </div>
                </div>
                <div class="line"></div>
                <footer>
            <p class="text-center">
            Traffic Fine Management System &copy;<?php echo date("Y ");?>Copyright. All Rights Reserved, Powered By Department of Computer Engineering,USJP    
            </p>
            </footer>
            </div>
            
        </div>





        <!-- jQuery CDN -->
         <script src="assets/js/jquery-1.10.2.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="assets/js/bootstrap.min.js"></script>

         <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                 });
             });
             $('sams').on('click', function(){
                 $('makota').addClass('animated tada');
             });
         </script>
         <script type="text/javascript">

        $(document).ready(function () {
 
            window.setTimeout(function() {
        $("#sams1").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
        });
            }, 5000);
 
        });
    </script>
    </body>
</html>
